(angular.module 'myApp')
.factory 'products', ->
  _.map _.range(10), ->
    name: Faker.Lorem.words(2).join(' ')
    price: Faker.random.number(200)
