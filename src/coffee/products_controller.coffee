(angular.module 'myApp').controller 'ProductsController', ($scope, products) ->
  $scope.products = _.map products, (p) -> name: p.name, price: p.price, quantity: 0
  $scope.grandTotal = ->
    _.reduce $scope.products, ((acc, p) -> acc + p.price * p.quantity), 0
