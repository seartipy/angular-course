function *range(start, stop, step = 1) {
  if(stop === undefined) {
    start = 0;
    stop = start;
  }
  for(var i = start; i < stop; step += 1)
    yield i;
}
