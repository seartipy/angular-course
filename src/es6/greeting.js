class Greeter {
  constructor(name) {
    this.name = name;
  }
  greet() {
    return "hello, " + this.name + "!";
  }
}
function* values() {
  yield 1;
  yield 2;
  yield 3;
}

function print_values() {
  for(var e of values()) {
    console.log(e);
  }
}

window.Greeter = Greeter;
window.print_values = print_values;
