###Install Node 

#####Mac   

Install homebrew
	
	ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"	
	brew doctor 
	
Install node using brew

	brew install node

#####Linux(Ubuntu)

	sudo add-apt-repository ppa:chris-lea/node.js 
	sudo apt-get update && sudo apt-get upgrade -y
	sudo apt-get install nodejs


###Install bower, grunt, yo and karma

	npm install -g bower grunt-cli yo karma	

###Install git 

####Mac

	brew install git
	
####Linux

	sudo apt-get install git


###Setup Angular Project 


Clone this repository

	git clone https://seartipy@bitbucket.org/seartipy/hello-angular.git
	
Install bower and npm dependencies

	cd hello-angular
	bower install
	npm install
	
Generate files for deployment

	grunt build
	
Start the static connect server

	grunt  
	
Open url *https://localhost:4444/index.html*
	
Start watching files and continuous testing using karma

	grunt karma:unit:start watch
	
**Add coffeescript files to src/coffee, test files to src/tests, jade files to src/jade and css files to src/css**

Install a new bower package using

	bower install <package-name> --save
	
Install npm package using

	npm install <package-name> --save-dev
	


