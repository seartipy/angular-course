module.exports = (grunt) ->
  (require 'load-grunt-tasks')(grunt)
  grunt.registerTask 'default', ['connect:server']
  grunt.registerTask 'tests', ['karma:unit:start', 'watch:karma']
  grunt.registerTask 'build', ['bower', 'coffee', 'traceur', 'jade', 'cssmin', 'uglify']

  grunt.initConfig
    pkg: grunt.file.readJSON('package.json')

    coffee:
      concat:
        expand: true
        cwd: 'src/coffee'
        src: ['*.coffee']
        dest: 'build/js'
        ext: '.coffee.js'

    jade:
      compile:
        expand: true
        cwd: 'src/jade'
        src: ['*.jade']
        dest: 'build/html/'
        ext: '.html'

    traceur:
      options:
        experimental: true #traceur options here
      custom:
        expand: true
        cwd: 'src/es6'
        src: ['*.js']
        dest: 'build/js'
        ext: '.es6.js'

    bower:
      install:
        options:
          targetDir: 'build/lib'

    cssmin:
      combine:
        files:
          'build/css/lib.min.css': ['build/lib/**/*.css']
          'build/css/all.min.css': ['src/css/*.css']

    uglify:
      libs:
        options:
          mangle: false
        files:
          'build/js/traceur-runtime.min.js': ['node_modules/grunt-traceur/node_modules/traceur/src/runtime/runtime.js']
          'build/js/jquery.min.js': ['build/lib/jquery/jquery.js']
          'build/js/lib.min.js': ['build/lib/**/!(jquery).js']

      coffee:
        files:
          'build/js/all.coffee.min.js': ['build/js/*.coffee.js']
        options:
          mangle: false
          sourceMappingURL: (s) ->
             'all.coffee.min.js.map'
          sourceMapPrefix: 2
          sourceMap: (s) -> s + '.map'

      traceur:
        files:
          'build/js/all.es6.min.js': ['build/js/*.es6.js']
        options:
          sourceMappingURL: (s) ->
             'all.es6.min.js.map'
          sourceMapPrefix: 2
          sourceMap: (s) -> s + '.map'
    karma:
      unit:
        configFile: 'karma.conf.coffee'
        background: true

    connect:
      server:
        options:
          port: 4444
          keepalive: true
          base: ['build/html', 'build/js', 'build/css']

    watch:
      bower:
        files: ['package.json'] # add bower_components?(for bower update)
        tasks: ['bower']
      build:
        files: ['Gruntfile.coffee']
        tasks: ['build']
      karma:
        files: ['src/coffee/*.coffee', 'src/tests/*.coffee']
        # use command : grunt karma:unit:start watch
        tasks: ['karma:unit:run']
      coffee:
        files: ['src/coffee/*.coffee']
        tasks: ['coffee']
      traceur:
        files: ['src/es6/*.js']
        tasks: ['traceur']
      jade:
        files: ['src/jade/*.jade']
        tasks: ['jade']
      uglify_libs:
        files: ['src/runtime.js', 'build/lib/**/*.js']
        tasks: ['uglify:libs']
      uglify_coffee:
        files: ['build/js/*.coffee.js']
        tasks: ['uglify:coffee']
      uglify_traceur:
        files: ['build/js/*.es6.js']
        tasks: ['uglify:traceur']
      cssmin:
        files: ['build/lib/**/*.js', 'src/css/*.css']
        tasks: ['cssmin']
      options:
        spawn: true
        livereload: true
